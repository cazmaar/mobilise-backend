import request from "supertest";
import app from "../app.js";
import pool from "../db/connection.js";

describe("Get user for the users route works as expected", () => {
  // testing the status code to see it returns the right status code
  test("should return a status of 200", async () => {
    //  arrange
    const res = await request(app).get("/users");
    // assert
    expect(res.statusCode).toBe(200);
  });
});

afterAll(() => pool.end());

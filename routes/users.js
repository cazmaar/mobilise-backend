import express from "express";
import { postDetails } from "../models/users.js";
const router = express.Router();

/* GET users listing. */
router.get("/", function (req, res, next) {
  res.json({ message: "I wish we had some information to give you ☹️fdfs" });
});

router.post("/", async function (req, res, next) {
  const body = req.body;
  const post = await postDetails(body);

  res.json({ message: "success", payload: post });
});

export default router;

import db from "../../connection.js";

const response = db.query(
  `CREATE TABLE IF NOT EXISTS details (id SERIAL PRIMARY KEY, name TEXT, job TEXT, age INT, location TEXT);`
);

console.log(await response);

db.end();
